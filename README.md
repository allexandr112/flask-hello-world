# flask-hello-world

## Table of content

<!-- toc -->

- [Ho to run locally](#ho-to-run-locally)
- [Accessing application remotely](#accessing-application-remotely)
- [Multiplatform docker build on MacOS](#multiplatform-docker-build-on-macos)

<!-- tocstop -->

## Ho to run locally
- Ensure you have [docker-compose](https://docs.docker.com/compose/) installed
- Inside project directory run
```
docker-compose build
docker-compose up -d
```
- You can access application via `http://localhost:9913`
- To stop application run `docker-compose down`

## Accessing application remotely
Application can be accessed via [this url](https://janbo.sandbox-las.cloudns.ph/).

## Multiplatform docker build on MacOS
You can use `buildx` tool:
1. (Optional) Create and switch to new build environment
```
docker buildx create --use --platform linux/arm64,linux/amd64
```
2. If you have already created it, you can switch to it
```
docker buildx ls
docker buildx use <name of your env>
```
3. Run docker build command (dev)
```
docker buildx build -f Dockerfile . --tag allexandr112/flask-hello-world:dev --push \
    --platform linux/arm64,linux/amd64
```