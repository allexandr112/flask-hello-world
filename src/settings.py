import os


def get_port():
    return os.getenv('SERVER_PORT', default=11130)
