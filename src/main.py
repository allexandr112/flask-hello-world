from flask import Flask
import settings as config

app = Flask(__name__)


@app.route('/')
def index():
    return 'Hello janbo from Aleksandr'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=config.get_port())